module.exports = {
  parser: 'vue-eslint-parser',

  parserOptions: {
    parser: '@typescript-eslint/parser',
    ecmaVersion: 2020,
    sourceType: 'module',
    ecmaFeatures: {
      jsx: true
    }
  },
  /* 继承某些已有的规则 */
  extends: ['plugin:vue/vue3-recommended', 'plugin:@typescript-eslint/recommended', 'prettier', 'plugin:prettier/recommended'],

  rules: {
    /*
     * "off" 或 0    ==>  关闭规则
     * "warn" 或 1   ==>  打开的规则作为警告（不影响代码执行）
     * "error" 或 2  ==>  规则作为一个错误（代码不能执行，界面报错）
     */
    // 禁止使用 @ts-ignore
    '@typescript-eslint/ban-ts-ignore': 'off',
    // 不允许对初始化为数字、字符串或布尔值的变量或参数进行显式类型声明
    '@typescript-eslint/explicit-function-return-type': 'off',
    // 禁止使用 any 类型
    '@typescript-eslint/no-explicit-any': 'off',
    // 不允许在 import 语句中使用 require 语句
    '@typescript-eslint/no-var-requires': 'off',
    // 禁止空函数
    '@typescript-eslint/no-empty-function': 'off',
    // 禁止在变量定义之前使用它们
    'no-use-before-define': 'off',
    '@typescript-eslint/no-use-before-define': 'off',
    // 禁止 @ts-<directive> 使用注释或要求在指令后进行描述
    '@typescript-eslint/ban-ts-comment': 'off',
    '@typescript-eslint/ban-types': 'off',
    '@typescript-eslint/no-non-null-assertion': 'off',
    '@typescript-eslint/explicit-module-boundary-types': 'off',
    'vue/multi-word-component-names': 'off',
    // 为自定义事件名称强制使用特定大小写
    'vue/custom-event-name-casing': 'off',
    // 防止<script setup>使用的变量
    'vue/script-setup-uses-vars': 'error',
    // 强制执行 v-slot 指令样式
    'vue/v-slot-style': 'off',
    // 不允许组件修改prop
    'vue/no-mutating-props': 'error',
    // vue api使用顺序，强制执行属性顺序
    'vue/attributes-order': 'off',
    // 在标签的右括号之前要求或禁止换行
    'vue/html-closing-bracket-newline': [
      'error',
      {
        singleline: 'never',
        multiline: 'always'
      }
    ],
    // 在单行元素的内容之前和之后需要换行符
    'vue/multiline-html-element-content-newline': [
      'error',
      {
        ignoreWhenEmpty: true,
        ignores: ['pre', 'textarea', ...INLINE_ELEMENTS],
        allowEmptyLines: false
      }
    ],
    // 对模板中的自定义组件强制执行属性命名样式
    'vue/attribute-hyphenation': 'off',

    // 禁止出现未使用过的变量
    '@typescript-eslint/no-unused-vars': [
      'error',
      {
        argsIgnorePattern: '^h$',
        varsIgnorePattern: '^h$'
      }
    ],
    'no-unused-vars': [
      'error',
      {
        argsIgnorePattern: '^h$',
        varsIgnorePattern: '^h$'
      }
    ],
    'space-before-function-paren': 'off',
    quotes: ['error', 'single'],
    'comma-dangle': ['error', 'never']
  },
  globals: {
    defineProps: 'readonly',
    defineEmits: 'readonly',
    defineExpose: 'readonly',
    withDefaults: 'readonly'
  }
};
