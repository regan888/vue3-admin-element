module.exports = {
  // 一行最多 130 字符
  printWidth: 130,
  // 使用 2 个空格缩进
  tabWidth: 2,
  // 使用 tab 缩进，而使用空格
  useTabs: false,
  // 行尾需要有分号
  semi: true,
  // Vue文件脚本和样式标签缩进
  vueIndentScriptAndStyle: true,
  // 使用单引号代替双引号
  singleQuote: true,
  // 对象的 key 仅在必要时用引号,可选值"<as-needed|consistent|preserve>"
  quoteProps: 'as-needed',
  // 大括号内的首尾需要空格 { foo: bar }
  bracketSpacing: true,
  // 末尾使用逗号
  trailingComma: 'none',
  // 箭头函数，只有一个形参的时候，是否需要括号，avoid：省略括号 ,always：不省略括号
  arrowParens: 'avoid',
  // 如果文件顶部已经有一个 doclock，这个选项将新建一行注释，并打上@format标记。
  insertPragma: false,
  // 指定要使用的解析器，不需要写文件开头的 @prettier
  requirePragma: false,
  // 使用默认的折行标准， 因为使用了一些折行敏感型的渲染器（如GitHub comment）而按照markdown文本样式进行折行
  proseWrap: 'never',
  // 根据显示样式决定 html 要不要折行
  htmlWhitespaceSensitivity: 'strict',
  // 换行符使用 lf
  endOfLine: 'auto'
};
