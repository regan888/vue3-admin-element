/*
 * @Author: ZhaoDong
 * @Date: 2022-07-07 16:26:22
 * @LastEditors: ZhaoDong
 * @LastEditTime: 2022-07-11 16:36:48
 * @Description: vite 插件
 */
import vue from '@vitejs/plugin-vue';
import vueJsx from '@vitejs/plugin-vue-jsx';
import vueSetupExtend from 'vite-plugin-vue-setup-extend';
import { PluginOption } from 'vite';
import { configSvgIconsPlugin } from './svgSprite';
import { configHtmlPlugin } from './html';
import { configCompressPlugin } from './compress';
import { configAutoImportPlugin } from './autoImport';
import { configInspectPlugin } from './Inspect';

export function createVitePlugins(viteEnv: ViteEnv, isBuild: boolean) {
  const { VITE_BUILD_COMPRESS, VITE_BUILD_COMPRESS_DELETE_ORIGIN_FILE } = viteEnv;

  const vitePlugins: (PluginOption | PluginOption[])[] = [
    vue(),
    // jsx
    vueJsx(),
    // vue3.2 step语法糖支持name
    vueSetupExtend(),
    // 自动导入Element ui 组件+图标
    configAutoImportPlugin()
  ];

  // vite-plugin-html
  vitePlugins.push(configHtmlPlugin(viteEnv, isBuild));

  // vite-plugin-svg-icons
  vitePlugins.push(configSvgIconsPlugin(isBuild));

  // The following plugins only work in the production environment
  if (isBuild) {
    // rollup-plugin-gzip
    vitePlugins.push(configCompressPlugin(VITE_BUILD_COMPRESS, VITE_BUILD_COMPRESS_DELETE_ORIGIN_FILE));
  }

  // vite-plugin-inspect 检查Vite插件的中间状态。对于调试和编写插件很有用。
  vitePlugins.push(configInspectPlugin(isBuild));

  return vitePlugins;
}
