/*
 * @Author: ZhaoDong
 * @Date: 2022-07-08 14:19:39
 * @LastEditors: ZhaoDong
 * @LastEditTime: 2022-07-08 14:23:44
 * @Description:  vite-plugin-inspect
 */
import type { PluginOption } from 'vite';
import Inspect from 'vite-plugin-inspect';
export function configInspectPlugin(isBuild: boolean): PluginOption {
  const plugins: PluginOption[] = [];
  if (!isBuild) {
    plugins.push(Inspect());
  }
  return plugins;
}
