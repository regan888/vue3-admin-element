/*
 * @Author: ZhaoDong
 * @Date: 2022-07-07 16:38:24
 * @LastEditors: ZhaoDong
 * @LastEditTime: 2022-07-08 15:25:02
 * @Description: 自动导入ElementPlus组件/Icon
 */
import path from 'path';
import type { PluginOption } from 'vite';
import { ElementPlusResolver } from 'unplugin-vue-components/resolvers';
import AutoImport from 'unplugin-auto-import/vite';
import Components from 'unplugin-vue-components/vite';
import IconsResolver from 'unplugin-icons/resolver';
import Icons from 'unplugin-icons/vite';
const pathSrc = path.resolve(process.cwd(), 'src');

export function configAutoImportPlugin(): PluginOption | PluginOption[] {
  const plugins: PluginOption[] = [
    AutoImport({
      // 自动导入 Vue 相关函数，如：ref, reactive, toRef 等
      imports: ['vue'],
      // 自动导入 Element Plus 相关函数，如：ElMessage, ElMessageBox... (带样式)
      resolvers: [
        ElementPlusResolver(),
        // 自动导入图标组件
        IconsResolver({
          prefix: 'Icon'
        })
      ],
      // dts: path.resolve(process.cwd(), 'auto-imports.d.ts') // path.resolve(__dirname, '/auto-imports2.d.ts')
      dts: path.resolve(pathSrc, 'typings', 'auto-imports.d.ts')
    }),

    Components({
      resolvers: [
        // 自动注册图标组件
        IconsResolver({
          enabledCollections: ['ep']
        }),
        // 自动导入 Element Plus 组件
        ElementPlusResolver()
      ],
      // dts: path.resolve(process.cwd(), 'components.d.ts')
      dts: path.resolve(pathSrc, 'typings', 'components.d.ts')
    }),
    Icons({
      autoInstall: true
    })
  ];

  return plugins;
}
