/*
 * @Author: ZhaoDong
 * @Date: 2022-07-07 16:35:58
 * @LastEditors: ZhaoDong
 * @LastEditTime: 2022-07-08 14:21:08
 * @Description: SVG ICON
 *  Vite Plugin for fast creating SVG sprites.
 *  https://github.com/anncwb/vite-plugin-svg-icons
 */
import type { PluginOption } from 'vite';
import { createSvgIconsPlugin } from 'vite-plugin-svg-icons';
import path from 'path';

export function configSvgIconsPlugin(isBuild: boolean): PluginOption {
  const svgIconsPlugin = createSvgIconsPlugin({
    iconDirs: [path.resolve(process.cwd(), 'src/assets/icons')],
    svgoOptions: isBuild,
    // 指定symbolId格式
    symbolId: 'icon-[dir]-[name]'
    /**
     * 自定义插入位置
     * @default: body-last
     */
    // inject?: 'body-last' | 'body-first'

    /**
     * custom dom id
     * @default: __svg__icons__dom__
     */
    // customDomId: '__svg__icons__dom__',
  });
  return svgIconsPlugin;
}
