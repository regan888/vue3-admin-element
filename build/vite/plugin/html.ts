/*
 * @Author: ZhaoDong
 * @Date: 2022-07-07 17:13:41
 * @LastEditors: ZhaoDong
 * @LastEditTime: 2022-07-12 12:09:14
 * @Description: HTML 压缩能力+EJS 模版能力+
 *  Plugin to minimize and use ejs template syntax in index.html.
 *  https://github.com/anncwb/vite-plugin-html
 */

import type { PluginOption } from 'vite';
import { createHtmlPlugin } from 'vite-plugin-html';
import pkg from '../../../package.json';
import { GLOB_CONFIG_FILE_NAME } from '../../constant';
import dayjs from 'dayjs';

export function configHtmlPlugin(env: ViteEnv, isBuild: boolean) {
  const { VITE_GLOB_APP_TITLE, VITE_PUBLIC_PATH } = env;

  const path = VITE_PUBLIC_PATH.endsWith('/') ? VITE_PUBLIC_PATH : `${VITE_PUBLIC_PATH}/`;
  const build_time = dayjs().format('YYYY-MM-DD HH:mm:ss');
  const getAppConfigSrc = () => {
    return `${path || '/'}${GLOB_CONFIG_FILE_NAME}?v=${pkg.version}-${new Date().getTime()}`;
  };

  const htmlPlugin: PluginOption[] = createHtmlPlugin({
    minify: isBuild,
    inject: {
      // Inject data into ejs template
      data: {
        title: VITE_GLOB_APP_TITLE,
        build_time
      },
      // Embed the generated app.config.js file
      tags: isBuild
        ? [
            {
              tag: 'script',
              attrs: {
                src: getAppConfigSrc()
              }
            }
          ]
        : []
    }
  });
  return htmlPlugin;
}
