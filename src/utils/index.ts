/*
 * @Author: ZhaoDong
 * @Date: 2022-07-08 18:52:58
 * @LastEditors: ZhaoDong
 * @LastEditTime: 2022-07-11 10:52:26
 * @Description: 基础工具合集
 */

/** 获取全局 CSS 变量 */
export const getCssVariableValue = (cssVariableName: string) => {
  let cssVariableValue = '';
  try {
    cssVariableValue = getComputedStyle(document.documentElement).getPropertyValue(cssVariableName);
  } catch (error) {
    console.error(error);
  }
  return cssVariableValue;
};
