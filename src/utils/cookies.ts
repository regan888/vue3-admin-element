/*
 * @Author: ZhaoDong
 * @Date: 2022-07-08 19:41:32
 * @LastEditors: ZhaoDong
 * @LastEditTime: 2022-07-08 19:43:50
 * @Description: Cookie 统一处理
 */
const token_key = 'admin_token';
import Cookies from 'js-cookie';

export const getToken = () => Cookies.get(token_key);
export const setToken = (token: string) => Cookies.set(token_key, token);
export const removeToken = () => Cookies.remove(token_key);
