/*
 * @Author: ZhaoDong
 * @Date: 2022-07-11 10:44:34
 * @LastEditors: ZhaoDong
 * @LastEditTime: 2022-07-11 10:55:25
 * @Description: 日期处理
 */
import dayjs from 'dayjs';

const DATE_TIME_FORMAT = 'YYYY-MM-DD HH:mm:ss';
const DATE_FORMAT = 'YYYY-MM-DD';

/**
 * 日期格式化（默认：年月日YYYY-MM-DD）
 * @param date 时间: string | number | Date | Dayjs | null | undefined
 * @param pattern 格式，默认: YYYY-MM-DD
 * @returns 字符串日期/时间
 */
export function formatDate(date: dayjs.Dayjs | undefined = undefined, pattern = DATE_FORMAT): string {
  return dayjs(date).format(pattern);
}

/**
 * 日期格式化（年月日时分秒）
 * @param date 时间
 * @param pattern 格式 YYYY-MM-DD HH:mm:ss
 * @returns YYYY-MM-DD HH:mm:ss
 */
export function formatToDateTime(date: dayjs.Dayjs | undefined = undefined, pattern = DATE_TIME_FORMAT): string {
  return dayjs(date).format(pattern);
}

/**
 * 日期格式化（年月日）
 * @param date 时间
 * @param pattern 格式 YYYY-MM-DD
 * @returns YYYY-MM-DD
 */
export function formatToDate(date: dayjs.Dayjs | undefined = undefined, pattern = DATE_FORMAT): string {
  return dayjs(date).format(pattern);
}

export const dateUtil = dayjs;
