/*
 * @Author: ZhaoDong
 * @Date: 2022-07-09 19:39:31
 * @LastEditors: ZhaoDong
 * @LastEditTime: 2022-07-10 23:39:10
 * @Description: Login接口
 */
import { request } from '/@/utils/axios';

/**
 * 登录
 * @param code 授权码
 * @returns {Promise<AxiosResponse<T>>}
 */
export async function login(code: string) {
  return request({
    url: '/api/receiving/oauth',
    method: 'post',
    data: {
      code
    }
  });
}

/** 登录并返回 Token */
// export function login(username: string, password: string) {
//   return request<IResponseType<ILogin>>({
//     url: 'users/login',
//     method: 'post',
//     data: {
//       username,
//       password
//     }
//   });
// }
/** 获取用户详情 */
export function getUserInfo() {
  return request({
    url: 'users/info',
    method: 'post'
  });
}
