/*
 * @Author: ZhaoDong
 * @Date: 2022-07-07 00:13:58
 * @LastEditors: ZhaoDong
 * @LastEditTime: 2022-07-19 18:58:57
 * @Description: routes
 */
import type { AppRouteRecordRaw } from '/@/router/types';
import type { RouteRecordRaw } from 'vue-router';
import { createRouter, createWebHistory } from 'vue-router';

export const LAYOUT = () => import('/@/layouts/default/index.vue');

// // * 导入所有router
// const metaRouters = import.meta.globEager("./modules/*.ts");

// // * 处理路由表
// export const routerArray: AppRouteRecordRaw[] = [];

// Object.keys(metaRouters).forEach(item => {
// 	Object.keys(metaRouters[item]).forEach((key: any) => {
// 		routerArray.push(...metaRouters[item][key]);
// 	});
// });

export const constantRoutes: Array<AppRouteRecordRaw> = [
  {
    path: '/',
    name: 'Index',
    redirect: '/dashboard',
    meta: {
      hidden: true,
      title: 'Index',
      hideBreadcrumb: true
    }
  },
  {
    path: '/redirect',
    component: LAYOUT,
    name: 'redirect',
    meta: {
      title: 'redirect',
      hidden: true,
      hideTab: true
    },
    children: [
      {
        name: 'redirectPage',
        path: '/redirect/:path(.*)',
        component: () => import('/@/pages/redirect/index.vue'),
        meta: {
          title: 'redirect',
          hidden: true,
          hideTab: true
        }
      }
    ]
  },
  {
    path: '/dashboard',
    name: 'Dashboard',
    component: LAYOUT,
    redirect: '/dashboard/analysis',
    meta: {
      hidden: true,
      orderNo: 10,
      icon: 'ion:grid-outline',
      title: '首页',
      hideBreadcrumb: true
    },
    children: [
      {
        path: 'analysis',
        name: 'Analysis',
        component: () => import('/@/pages/dashboard/analysis/index.vue'),
        meta: {
          affix: true,
          icon: 'HomeFilled',
          title: '首页'
        }
      }
    ]
  },
  {
    path: '/login',
    name: 'Login',
    meta: {
      title: '登录',
      keepAlive: false,
      hidden: true,
      hideTab: true
    },
    component: () => import('/@/pages/login.vue')
  },
  {
    path: '/:pathMatch(.*)',
    name: 'error',
    redirect: '/404',
    meta: {
      hidden: true,
      hideTab: true,
      title: 'error'
    },
    children: [
      {
        path: '/403',
        name: '403',
        component: () => import('/@/components/Error/403.vue'),
        meta: {
          title: '403页面'
        }
      },
      {
        path: '/404',
        name: '404',
        component: () => import('/@/components/Error/404.vue'),
        meta: {
          title: '404页面'
        }
      },
      {
        path: '/500',
        name: '500',
        component: () => import('/@/components/Error/500.vue'),
        meta: {
          title: '500页面'
        }
      }
    ]
  }
];

export const asyncRoutes: Array<AppRouteRecordRaw> = [
  {
    path: '/menu',
    component: LAYOUT,
    redirect: '/menu/menu1',
    name: 'Menu',
    meta: {
      title: '多级菜单',
      // icon: 'Position'
      svgIcon: 'sun'
    },
    children: [
      {
        path: 'menu1',
        component: () => import('/@/pages/menu/menu1/index.vue'),
        redirect: '/menu/menu1/menu1-1',
        name: 'Menu1',
        meta: { title: 'menu1', icon: 'Promotion' },
        children: [
          {
            path: 'menu1-1',
            component: () => import('/@/pages/menu/menu1/menu1-1/index.vue'),
            name: 'Menu1-1',
            meta: { title: 'menu1-1', icon: 'Promotion' }
          },
          {
            path: 'menu1-2',
            component: () => import('/@/pages/menu/menu1/menu1-2/index.vue'),
            redirect: '/menu/menu1/menu1-2/menu1-2-1',
            name: 'Menu1-2',
            meta: { title: 'menu1-2' },
            children: [
              {
                path: 'menu1-2-1',
                component: () => import('/@/pages/menu/menu1/menu1-2/menu1-2-1/index.vue'),
                name: 'Menu1-2-1',
                meta: { title: 'menu1-2-1' }
              },
              {
                path: 'menu1-2-2',
                component: () => import('/@/pages/menu/menu1/menu1-2/menu1-2-2/index.vue'),
                name: 'Menu1-2-2',
                meta: { title: 'menu1-2-2' }
              }
            ]
          },
          {
            path: 'menu1-3',
            component: () => import('/@/pages/menu/menu1/menu1-3/index.vue'),
            name: 'Menu1-3',
            meta: { title: 'menu1-3' }
          }
        ]
      },
      {
        path: 'menu2',
        component: () => import('/@/pages/menu/menu2/index.vue'),
        name: 'Menu2',
        meta: { title: 'SvgIcon', svgIcon: 'sun' }
      }
    ]
  },
  {
    path: '/link',
    component: LAYOUT,
    meta: {
      title: 'Link',
      authority: 'xxxxxx'
    },
    name: 'link',
    children: [
      {
        path: 'http://oauth-oa.yuzhua-test.com/',
        component: () => {},
        name: 'Link2',
        meta: {
          title: '外链(OA)',
          icon: 'link',
          authority: 'axxxxx'
        }
      }
    ]
  }
];

const router = createRouter({
  history: createWebHistory(),
  routes: constantRoutes as unknown as RouteRecordRaw[],
  scrollBehavior: () => ({ left: 0, top: 0 })
});
export default router;
