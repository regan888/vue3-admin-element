/*
 * @Author: ZhaoDong
 * @Date: 2022-07-08 15:42:14
 * @LastEditors: ZhaoDong
 * @LastEditTime: 2022-07-08 15:43:19
 * @Description: El-Icon
 */
import type { App } from 'vue';
import * as Icons from '@element-plus/icons-vue';
export default {
  install: (app: App) => {
    for (const key in Icons) {
      app.component(key, Icons[key as keyof typeof Icons]);
    }
  }
};
