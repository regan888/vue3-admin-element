/*
 * @Author: ZhaoDong
 * @Date: 2022-07-19 17:35:14
 * @LastEditors: ZhaoDong
 * @LastEditTime: 2022-07-19 17:58:24
 * @Description: 自定义指令
 */
import type { Directive, App } from 'vue';
import copy from './modules/copy';
import debounce from './modules/debounce';
import throttle from './modules/throttle';
import longpress from './modules/longpress';

const directivesList: { [directiveName: string]: Directive } = {
  copy,
  debounce,
  throttle,
  longpress
};

const directives = {
  install: function (app: App<Element>) {
    Object.keys(directivesList).forEach(key => {
      app.directive(key, directivesList[key]);
    });
  }
};

export default directives;
