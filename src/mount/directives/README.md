```javascript
app.directive('directiveName', {
  created(el, binding, vnode, prevVnode) {},
  // 当指令第一次绑定到元素并且在挂载父组件之前调用
  beforeMount(el) {},
  // 在绑定元素的父组件被挂载后调用
  mounted(el, binding) {},
  // 更新包含组件的 VNode 之前调用
  beforeUpdate(el) {},
  // 在包含组件的 VNode 及其子组件的 VNode 更新后调用
  updated(el) {},
  // 在卸载绑定元素的父组件之前调用
  beforeUnmount(el) {},
  // 当指令与元素解除绑定且父组件已卸载时，只调用一次。
  unmounted(el) {}
});
```

> el：指令绑定到的元素。这可以用于直接操作 DOM。

> binding：一个对象，包含以下 property：

    1. value：传递给指令的值。例如在 v-my-directive=“1 + 1” 中，值是 2。
    2. oldValue：之前的值，仅在 beforeUpdate 和 updated 中可用。无论值是否更改，它都可用。
    3. arg：传递给指令的参数 (如果有的话)。例如在 v-my-directive:foo 中，参数是 “foo”。
    4. modifiers：一个包含修饰符的对象 (如果有的话)。例如在 v-my-directive.foo.bar 中，修饰符对象是 { foo: true, bar: true }。
    5. instance：使用该指令的组件实例。
    6. dir：指令的定义对象。

> vnode：代表绑定元素的底层 VNode。

> prevNode：之前的渲染中代表指令所绑定元素的 VNode。仅在 beforeUpdate 和 updated 钩子中可用。
