/*
 * @Author: ZhaoDong
 * @Date: 2022-07-06 23:26:45
 * @LastEditors: ZhaoDong
 * @LastEditTime: 2022-07-08 17:31:19
 * @Description: pinia
 */
import { createPinia } from 'pinia';

const store = createPinia();

export default store;
