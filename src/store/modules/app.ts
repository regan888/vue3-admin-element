/*
 * @Author: ZhaoDong
 * @Date: 2022-07-08 17:16:09
 * @LastEditors: ZhaoDong
 * @LastEditTime: 2022-07-10 21:17:52
 * @Description: appStore
 */
import { defineStore } from 'pinia';

export enum DeviceType {
  Mobile,
  Desktop
}
export interface IAppStore {
  device: DeviceType;
  sidebar: {
    opened: boolean;
    withoutAnimation: boolean;
  };
}

export const useAppStore = defineStore({
  id: 'app',
  state: (): IAppStore => ({
    device: DeviceType.Desktop,
    sidebar: {
      opened: true,
      withoutAnimation: false
    }
  }),
  getters: {
    opened(): boolean {
      return this.sidebar.opened;
    }
  },
  actions: {
    closeSidebar(withoutAnimation: boolean) {
      this.sidebar.opened = false;
      this.sidebar.withoutAnimation = withoutAnimation;
    },
    toggleDevice(device: DeviceType) {
      this.device = device;
    },
    toggleSidebar(withoutAnimation: boolean) {
      this.sidebar.opened = !this.sidebar.opened;
      this.sidebar.withoutAnimation = withoutAnimation;
    }
  }
});
