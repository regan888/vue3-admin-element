import store from '/@/store';
import { defineStore } from 'pinia';
import { RouteRecordRaw } from 'vue-router';
import { constantRoutes, asyncRoutes } from '/@/router';
import { useUserStoreHook } from './user';
import type { AppRouteRecordRaw } from '/@/router/types';

interface IPermissionState {
  routes: RouteRecordRaw[];
  dynamicRoutes: RouteRecordRaw[];
}

// type authorityType = string | string[];

const hasAuthority = (route: RouteRecordRaw): boolean => {
  const { permissions, isAdmin } = useUserStoreHook();
  if (isAdmin) {
    return true;
  }
  if (route.meta && route.meta.authority) {
    const { authority } = route.meta;
    if (typeof authority === 'string') {
      return !!permissions[authority];
    } else if (Array.isArray(authority)) {
      return authority.some(item => permissions[item]);
    }
  }
  return true;
};

// const hasPermission = (roles: string[], route: RouteRecordRaw) => {
//   if (route.meta && route.meta.authority) {
//     return roles.some((role) => {
//       if (route.meta?.roles !== undefined && Array.isArray(route.meta.roles)) {
//         return route.meta.roles.includes(role);
//       } else {
//         return false;
//       }
//     });
//   } else {
//     return true;
//   }
// };

const filterAsyncRoutes = (routes: AppRouteRecordRaw[]) => {
  const routers: AppRouteRecordRaw[] = [];
  routes.forEach(route => {
    const r = { ...route };
    if (hasAuthority(r)) {
      if (r.children) {
        r.children = filterAsyncRoutes(r.children);
      }
      routers.push(r);
    }
  });
  return routers;
};

export const usePermissionStore = defineStore({
  id: 'permission',
  state: (): IPermissionState => {
    return {
      routes: [],
      dynamicRoutes: []
    };
  },
  actions: {
    setRoutes() {
      const accessedRoutes = filterAsyncRoutes(asyncRoutes);
      this.routes = constantRoutes.concat(accessedRoutes);
      this.dynamicRoutes = accessedRoutes;
    }
  }
});

/** 在 setup 外使用 */
export function usePermissionStoreHook() {
  return usePermissionStore(store);
}
