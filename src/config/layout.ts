/*
 * @Author: ZhaoDong
 * @Date: 2022-07-08 19:08:47
 * @LastEditors: ZhaoDong
 * @LastEditTime: 2022-07-11 11:33:48
 * @Description: 导航栏配置
 */
/** 布局配置 */
interface ILayoutSettings {
  /** 是否显示 Settings Panel */
  showSettings: boolean;
  /** 是否显示标签栏 */
  showTagsView: boolean;
  /** 是否显示侧边栏 Logo */
  showSidebarLogo: boolean;
  /** 是否固定 Header */
  fixedHeader: boolean;
}

const layoutSettings: ILayoutSettings = {
  showSettings: true,
  showTagsView: true,
  fixedHeader: true,
  showSidebarLogo: true
};

export default layoutSettings;
