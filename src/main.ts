/*
 * @Author: ZhaoDong
 * @Date: 2022-07-09 19:39:31
 * @LastEditors: ZhaoDong
 * @LastEditTime: 2022-07-19 18:19:01
 * @Description: MAIN
 */
import { createApp } from 'vue';
import App from './App.vue';
// import { setupStore } from '/@/store';
import store from '/@/store';
import router from '/@/router';
// svg封装插件
import SvgIcon from '/@/components/Icon/SvgIcon/index.vue';
import elIcon from '/@/mount/elIcon/index';
import 'virtual:svg-icons-register';
import '/@/router/permission';
import '/@/styles/index.scss';
import directives from '/@/mount/directives/index';

// 按需引入了
// import ElementPlus from 'element-plus';

const app = createApp(App);

// 配置 store
// setupStore(app);
app.use(store);

app.use(router);

app.use(directives);

// element-plus/icons-vue
app.use(elIcon);

app.component('SvgIcon', SvgIcon);

// app.use(ElementPlus);

app.mount('#app');
