# Vue 3 + TypeScript + Vite

> 本项目使用`pnpm`管理依赖包

## 使用 npm 安装 pnpm

> npm - 速度快、节省磁盘空间的软件包管理器, [戳这里了解更多](https://www.pnpm.cn/installation)

```bash
npm install -g pnpm
```

## npm/yarn/pnpm 的常用命令

| #           | npm                      | yarn                      | pnpm                  |
| ----------- | ------------------------ | ------------------------- | --------------------- |
| Install all | npm install              | yarn                      | pnpm install          |
| Install     | npm install [package]    | yarn add [package]        | pnpm add [package]    |
| Install Dev | npm install [package] -D | yarn add [package] -D     | pnpm add -D [package] |
| 全局安装    | npm install [package] -g | yarn global add [package] | pnpm add -g [package] |
| Uninstall   | npm uninstall [package]  | yarn remove [package]     | pnpm remove [package] |
| Update      | npm update [package]     | yarn upgrade [package]    | pnpm update [package] |

> 该项目模板说明[点这里](https://kj7uf5u0kn.feishu.cn/docx/doxcnidda71QFseznU4Tn2lftBe)

## 该模板内置功能如下

. 基于 OA 登录/登出

. 前端权限（动态路由、指令权限、权限函数）

. 路由守卫

. 请求拦截

. 多种环境配置（开发、预发布、正式）

. SVG & SvgIcon 组件

. 动态侧边栏、动态面包屑

. 动态导入 Element 组件

. 动态导入 vue 相关 API 如：ref, reactive, toRef 等

## SvgIcon 图标

> 1. 将 svg 文件统一放到 src\assets\icons 目录下

```html
<SvgIcon name="sun" color="#ec4b4b" :size="22" />
```

> 2. Element-Plus ICON 自动导入

- 使用 unplugin-icons 和 unplugin-auto-import 从 iconify 中自动导入任何图标集

安装

```
pnpm install unplugin-icons unplugin-auto-import -D
```

使用

```html
<el-icon><AlarmClock /></el-icon>
```

## 本地启动

```bash
pnpm dev
```

## 构建测试环境

```bash
pnpm build:test
```

## 构建正式环境

```bash
pnpm build
```

## 代码提交

```bash
pnpm commit
```
